//  Copyright © 2015 Under Armour. All rights reserved.

import UIKit
import StepsFoundation

// MARK:- Properties and Initializers
class ThisWeekViewController: UIViewController {
    @IBOutlet weak var walkTimeRecommendationLabel: UILabel!
    @IBOutlet weak var goForAWalkLabel: UILabel!
    @IBOutlet weak var recommendationView: UIVisualEffectView!
    @IBOutlet weak var recommendedTimeView: UIVisualEffectView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var tableViewController: ThisWeekStepsTableViewController?
    override var title: String? {
        get { return "This Week" }
        set { }
    }
    var dataSource: protocol<SynchronousStepDataSource, AsynchronousStepDataSource>!
    var dateFormatter: StepsDateFormatter!
    var outcome: ( (selectedDate: NSDate, dayOfWeek: String)->() )?
    var weekStats: StepCountStats? {
        didSet {
            if let stats = weekStats {
                goForAWalkLabel.text = "Go for a walk between"
                walkTimeRecommendationLabel.text = displayableStringForTimeWindow((from: stats.from, to:stats.to),
                                                                                  dateFormatter: dateFormatter)
            } else { // Super active individual
                goForAWalkLabel.text = "This week you were super active."
                walkTimeRecommendationLabel.text = "Keep it up!"
            }
        }
    }
}

// MARK:- View Controller Lifecycle
extension ThisWeekViewController {
    override func viewDidLoad() {
        assert(dataSource != nil)
        assert(dateFormatter != nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let tableViewController = tableViewController {
            var contentInset = tableViewController.tableView.contentInset
            contentInset.bottom = recommendationView.frame.size.height
            tableViewController.tableView.contentInset = contentInset
        }
    }
}

// MARK:- User Interaction
extension ThisWeekViewController {
    @IBAction func recommendWalkTime(findBestTimeButton: UIButton) {
        weekStats = dataSource.stepCountStatsForThisWeek()
        if weekStats == nil {
            // Animate Activity Indicator
            activityIndicator.startAnimating()
            UIView.animateWithDuration(0.2) {
                findBestTimeButton.alpha = 0
                self.activityIndicator.alpha = 1
            }
            // Request Recommendation
            let action = dataSource.createGetStepCountStatsForThisWeekAction()
            action.completionBlock = { [weak self, weak action] in
                if let strongSelf = self, let strongAction = action {
                    dispatch_async(dispatch_get_main_queue()) {
                        strongSelf.handleRecommendationResult(strongAction.stats)
                    }
                }
            }
            dataSource.scheduleGetStepCountStatsForThisWeekAction(action)
        } else {
            findBestTimeButton.alpha = 0
            animateInWalkTimeRecommendation()
        }
    }
    
    func handleRecommendationResult(resultStats: StepCountStats?) {
        if let stats = resultStats {
            if stats.from.endDateHour < 0 {
                goForAWalkLabel.text = "Error occurred"
                walkTimeRecommendationLabel.text = "Could not determine walk time."
                animateInWalkTimeRecommendation()
                return
            }
        }
        weekStats = resultStats
        animateInWalkTimeRecommendation()
    }
    
    func animateInWalkTimeRecommendation() {
        UIView.animateWithDuration(0.2,
            animations: {
                self.activityIndicator.alpha = 0
            },
            completion: { completed in
                self.recommendedTimeView.hidden = false
                self.activityIndicator.stopAnimating()
        })
    }
}

//MARK:- Child View Controller Methods
extension ThisWeekViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "stepsTableViewEmbed" {
            let tableViewController = segue.destinationViewController as! ThisWeekStepsTableViewController
            setUpStepsTableViewController(tableViewController)
            self.tableViewController = tableViewController
        }
    }
    
    func setUpStepsTableViewController(viewController: ThisWeekStepsTableViewController) {
        viewController.dataSource = dataSource
        viewController.dateFormatter = dateFormatter
        viewController.outcome = { [weak self] selectedDate, dayOfWeek in
            if let strongSelf = self {
                strongSelf.outcome?(selectedDate: selectedDate, dayOfWeek: dayOfWeek)
            }
        }
    }
}

// MARK:- Display Formatting
extension ThisWeekViewController {
    func displayableStringForTimeWindow(window: ActivityTimeWindow, dateFormatter: StepsDateFormatter) -> String {
        let components = window.from.componenetsBySubtractingThirtyMinutes()
        let lhsDate = NSDate.dateWithHour(components.hour, minute: components.minute)
        let rhsDate = NSDate.dateWithHour(window.to.endDateHour, minute: window.to.endDateMinute)
        let lhsString = dateFormatter.shortTimeStringFromDate(lhsDate)
        let rhsString = dateFormatter.shortTimeStringFromDate(rhsDate)
        return "\(lhsString) - \(rhsString)"
    }
}
