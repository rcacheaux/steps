//  Copyright © 2015 Under Armour. All rights reserved.

import UIKit

class DayStepCountCell: UITableViewCell {
    var date: NSDate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        date = nil
    }
}
