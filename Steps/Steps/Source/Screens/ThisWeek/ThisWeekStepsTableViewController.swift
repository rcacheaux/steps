//  Copyright © 2015 Under Armour. All rights reserved.

import UIKit
import StepsFoundation

// MARK:- Properties and Initializers
class ThisWeekStepsTableViewController: UITableViewController {
    var dataSource: protocol<SynchronousStepDataSource, AsynchronousStepDataSource>!
    var dateFormatter: StepsDateFormatter!
    var outcome: ( (selectedDate: NSDate, dayOfWeek: String)->() )?
    var days = [NSDate]()
    var dayOfWeek = [String]()
    var dateStrings = [String]()
    var actionMap = [NSDate:GetStepCountForDayAction]()
}

// MARK:- View Controller Lifecycle
extension ThisWeekStepsTableViewController {
    override func viewDidLoad() {
        assert(dataSource != nil)
        assert(dateFormatter != nil)
        days = WeekDateFactory.datesForThisWeek()
        dayOfWeek = days.map { dateFormatter.dayOfWeekStringFromDate($0) }
        dateStrings = days.map { dateFormatter.shortDateStringFromDate($0) }
    }
}

// MARK:- Table View Datasource
extension ThisWeekStepsTableViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return days.count
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("stepCountCell") as! DayStepCountCell
        
        let date = days[inverseRow(indexPath)]
        cell.date = date
        if let stepCount = dataSource.stepCountForDayWithDate(date) {
            cell.textLabel?.text = "\(stepCount)"
        } else if actionMap[date] == nil {
            let action = dataSource.createGetStepCountForDayActionWithDate(date)
            action.completionBlock = { [weak self, weak action, weak cell] in
                if let strongSelf = self, let strongAction = action, let strongCell = cell {
                    dispatch_async(dispatch_get_main_queue()) {
                        strongSelf.updateCellWithResultFromAction(strongAction, cell: strongCell)
                    }
                }
            }
            actionMap[date] = action
            dataSource.scheduleGetStepCountForDayActionWithDate(action)
        }
        
        cell.detailTextLabel?.text = "\(dayOfWeek[inverseRow(indexPath)]) \(dateStrings[inverseRow(indexPath)])"
        cell.accessoryType = .DisclosureIndicator
        cell.separatorInset = UIEdgeInsetsZero
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        outcome?(selectedDate: days[inverseRow(indexPath)], dayOfWeek: dayOfWeek[inverseRow(indexPath)])
    }
    
    func updateCellWithResultFromAction(action: GetStepCountForDayAction, cell: DayStepCountCell) {
        if let cellDate = cell.date {
            if action.date == cellDate {
                if let stepCount = action.stepCount {
                    cell.textLabel?.text = "\(stepCount)"
                }
            }
        }
    }
    
    func inverseRow(indexPath: NSIndexPath) -> Int {
        return (days.count - 1) - indexPath.row
    }
}


