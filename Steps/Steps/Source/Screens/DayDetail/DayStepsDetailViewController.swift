//  Copyright © 2015 Under Armour. All rights reserved.

import UIKit
import StepsFoundation

// MARK:- Properties and Initializers
class DayStepsDetailViewController: UITableViewController {
    @IBOutlet weak var numberOfStepsLabel: UILabel!
    var dataSource: protocol<SynchronousStepDataSource, AsynchronousStepDataSource>!
    var samples = StepCountSamples(intervalEndDates: [], stepCountMap: [:], activityLevelSnapshotSetMap: [:])
    var dateFormatter: StepsDateFormatter!
    var date: NSDate!
    var numberOfSteps: Int? {
        didSet {
            if let steps = numberOfSteps {
                numberOfStepsLabel.text = "\(steps)"
            } else {
                numberOfStepsLabel.text = ""
            }
        }
    }
    var dayOfWeek: String! {
        didSet {
            title = "\(dayOfWeek)"
        }
    }
}

// MARK:- View Controller Lifecycle
extension DayStepsDetailViewController {
    override func viewDidLoad() {
        assert(dataSource != nil)
        assert(dateFormatter != nil)
        assert(date != nil)
        assert(dayOfWeek != nil)
        
        numberOfSteps = dataSource.stepCountForDayWithDate(date)
        
        if let dataSourceSamples = dataSource.stepCountSamplesForDayWithDate(date) {
            samples = dataSourceSamples
        } else {
            let action = dataSource.createGetStepCountSamplesForDayActionWithDate(date)
            action.completionBlock = { [weak action, weak self] in
                if let strongAction = action, let strongSelf = self {
                    dispatch_async(dispatch_get_main_queue()) {
                        if let dataSourceSamples = strongAction.stepCountSamples {
                            strongSelf.samples = dataSourceSamples
                            strongSelf.tableView.reloadData()
                        }
                    }
                }
            }
            dataSource.scheduleGetStepCountSamplesForDayActionWithDate(action)
        }
    }
}

// MARK:- Table View Datasource
extension DayStepsDetailViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return samples.intervalEndDates.count
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("stepCountSampleCell")!
        let date = samples.intervalEndDates[indexPath.row]
        let stepCountOrNil = samples.stepCountMap[date]
        if let stepCount = stepCountOrNil {
            cell.detailTextLabel?.text = "\(stepCount.count) \(stepCount.level)"
        }
        cell.textLabel?.text = dateFormatter.shortTimeStringFromDate(date)
        return cell
    }
}
