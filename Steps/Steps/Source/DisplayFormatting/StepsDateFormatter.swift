//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

func createDateFormatter(onComplete: (dateFormatter: StepsDateFormatter)->()) {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) {
        let dateFormatter = StepsDateFormatter()
        dispatch_async(dispatch_get_main_queue()) {
            onComplete(dateFormatter: dateFormatter)
        }
    }
}

class StepsDateFormatter {
    private let dayOfWeekFormatter: NSDateFormatter
    private let shortDateFormatter: NSDateFormatter
    private let shortTimeDateFormatter: NSDateFormatter
    
    private init() {
        dayOfWeekFormatter = NSDateFormatter()
        dayOfWeekFormatter.dateFormat = "EEEE"
        shortDateFormatter = NSDateFormatter()
        shortDateFormatter.dateFormat = "MMM d"
        shortTimeDateFormatter = NSDateFormatter()
        shortTimeDateFormatter.timeStyle = .ShortStyle
        shortTimeDateFormatter.dateStyle = .NoStyle
    }
    
    func dayOfWeekStringFromDate(date: NSDate) -> String {
        return dayOfWeekFormatter.stringFromDate(date)
    }
    
    func shortDateStringFromDate(date: NSDate) -> String {
        return shortDateFormatter.stringFromDate(date)
    }
    
    func shortTimeStringFromDate(date: NSDate) -> String {
        return shortTimeDateFormatter.stringFromDate(date)
    }
}

