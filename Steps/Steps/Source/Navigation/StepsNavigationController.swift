//  Copyright © 2015 Under Armour. All rights reserved.

import UIKit
import StepsFoundation

enum ViewControllerId: String {
    case thisWeekViewController = "ThisWeekViewController"
    case dayStepsDetailViewController = "DayStepsDetailViewController"
}

// MARK:- Properties and Initializers
class StepsNavigationViewController: UINavigationController {
    #if arch(x86_64) || arch(i386)
    let dataSource: protocol<SynchronousStepDataSource, AsynchronousStepDataSource> = FakeDataStepDataSource()
    #else
    let dataSource: protocol<SynchronousStepDataSource, AsynchronousStepDataSource> = CoreMotionPedometerStepDataSource()
    #endif
    var dateFormatter: StepsDateFormatter?
    var hasPresentedError = false
}

// MARK:- View Controller Lifecycle
extension StepsNavigationViewController {
    override func viewDidLoad() {
        assert(storyboard != nil)

        // Set up on error closure
        dataSource.onError = { [weak self] error in
            if let strongSelf = self {
                dispatch_async(dispatch_get_main_queue()) {
                    strongSelf.presentError(error)
                }
            }
        }
        
        // Set up initial view controller and warm up date formatter
        let thisWeekViewController = createThisWeekViewController()
        createDateFormatter { dateFormatter in // Create NSDateFormatters in background and present UI once complete
            self.dateFormatter = dateFormatter
            thisWeekViewController.dateFormatter = dateFormatter
            self.pushViewController(thisWeekViewController, animated: false)
        }
    }
}

// MARK:- Navigation
extension StepsNavigationViewController {
    func navigateToDayDetail(selectedDate selectedDate: NSDate, dayOfWeek: String) {
        pushViewController(createDayStepDetailViewController(selectedDate: selectedDate, dayOfWeek: dayOfWeek), animated: true)
    }
}

// MARK:- Content View Controller Factories
extension StepsNavigationViewController {
    func createDayStepDetailViewController(selectedDate selectedDate: NSDate, dayOfWeek: String) -> DayStepsDetailViewController {
        let dayStepsDetailViewController = storyboard!.instantiateViewControllerWithIdentifier(
            ViewControllerId.dayStepsDetailViewController.rawValue) as! DayStepsDetailViewController
        dayStepsDetailViewController.dataSource = dataSource
        dayStepsDetailViewController.dateFormatter = dateFormatter
        dayStepsDetailViewController.date = selectedDate
        dayStepsDetailViewController.dayOfWeek = dayOfWeek
        return dayStepsDetailViewController
    }
    
    func createThisWeekViewController() -> ThisWeekViewController {
        let thisWeekViewController = storyboard!.instantiateViewControllerWithIdentifier(
            ViewControllerId.thisWeekViewController.rawValue) as! ThisWeekViewController
        thisWeekViewController.dataSource = dataSource
        thisWeekViewController.outcome = { [weak self] selectedDate, dayOfWeek in
            if let strongSelf = self {
                strongSelf.navigateToDayDetail(selectedDate: selectedDate, dayOfWeek: dayOfWeek)
            }
        }
        return thisWeekViewController
    }
}

// MARK:- Error Handling
extension StepsNavigationViewController {
    func presentError(error: NSError) {
        if hasPresentedError { return }
        hasPresentedError = true
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil) })
        alertController.addAction(action)
        presentViewController(alertController, animated: false, completion: nil)
    }
}
