//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

public protocol SynchronousStepDataSource: class {
    var onError: ( (NSError)->() )? { get set }
    
    func stepCountForDayWithDate(date: NSDate) -> Int?
    func stepCountSamplesForDayWithDate(date: NSDate) -> StepCountSamples?
    func stepCountStatsForThisWeek() -> StepCountStats?
}

public protocol AsynchronousStepDataSource: class {
    func createGetStepCountForDayActionWithDate(date: NSDate) -> GetStepCountForDayAction
    func createGetStepCountSamplesForDayActionWithDate(date: NSDate) -> GetStepCountSamplesForDayAction
    func createGetStepCountStatsForThisWeekAction() -> GetStepCountStatsForThisWeekAction
    
    func scheduleGetStepCountForDayActionWithDate(action: GetStepCountForDayAction)
    func scheduleGetStepCountSamplesForDayActionWithDate(action: GetStepCountSamplesForDayAction)
    func scheduleGetStepCountStatsForThisWeekAction(action: GetStepCountStatsForThisWeekAction)
}

extension SynchronousStepDataSource {
    public func createGetStepCountForDayActionWithDate(date: NSDate) -> GetStepCountForDayAction {
        return GetStepCountForDayAction(date: date, dataSource: self)
    }
    
    public func createGetStepCountSamplesForDayActionWithDate(date: NSDate) -> GetStepCountSamplesForDayAction {
        return GetStepCountSamplesForDayAction(date: date, dataSource: self)
    }
    
    public func createGetStepCountStatsForThisWeekAction() -> GetStepCountStatsForThisWeekAction {
        return GetStepCountStatsForThisWeekAction(dataSource: self)
    }
}

public class GetStepCountForDayAction: AsyncAction {
    private weak var dataSource: SynchronousStepDataSource?
    public let date: NSDate
    public private(set) var stepCount: Int?
    
    private init(date: NSDate, dataSource: SynchronousStepDataSource) {
        self.date = date
        self.dataSource = dataSource
        super.init()
    }
    
    public override func run() {
        dispatch_async(dispatch_get_main_queue()) {
            self.stepCount = self.dataSource?.stepCountForDayWithDate(self.date)
            self.finishExecutingOperation()
        }
    }
}

public class GetStepCountSamplesForDayAction: AsyncAction {
    private weak var dataSource: SynchronousStepDataSource?
    public let date: NSDate
    public private(set) var stepCountSamples: StepCountSamples?
    
    private init(date: NSDate, dataSource: SynchronousStepDataSource) {
        self.date = date
        self.dataSource = dataSource
        super.init()
    }
    
    public override func run() {
        dispatch_async(dispatch_get_main_queue()) {
            self.stepCountSamples = self.dataSource?.stepCountSamplesForDayWithDate(self.date)
            self.finishExecutingOperation()
        }
    }
}

public class GetStepCountStatsForThisWeekAction: AsyncAction {
    private weak var dataSource: SynchronousStepDataSource?
    public private(set) var stats: StepCountStats?
    
    private init(dataSource: SynchronousStepDataSource) {
        self.dataSource = dataSource
        super.init()
    }
    
    public override func run() {
        dispatch_async(dispatch_get_main_queue()) {
            self.stats = self.dataSource?.stepCountStatsForThisWeek()
            self.finishExecutingOperation()
        }
    }
}
