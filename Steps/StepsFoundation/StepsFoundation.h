//  Copyright © 2015 Under Armour. All rights reserved.

@import Foundation;
@import Elements;

//! Project version number for StepsFoundation.
FOUNDATION_EXPORT double StepsFoundationVersionNumber;

//! Project version string for StepsFoundation.
FOUNDATION_EXPORT const unsigned char StepsFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StepsFoundation/PublicHeader.h>
