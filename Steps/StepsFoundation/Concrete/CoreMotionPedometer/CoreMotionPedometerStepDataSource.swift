//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation
import CoreMotion

typealias PedometerDataResult = Result<CMPedometerData>

// MARK:- State and initializers
public class CoreMotionPedometerStepDataSource {
    private var stepCountForDayMap = [NSDate:Int]()
    private var stepCountSamplesForDayMap = [NSDate:StepCountSamples]()
    private var recommendedWalkWindow: ActivityTimeWindowResult?
    
    private let serialQueue = dispatch_queue_create("com.underarmour.stepsfoundation.coremotion_pedometer_stepdatasource",
                                                    DISPATCH_QUEUE_SERIAL)
    
    private var dayStepCountQueryActionMap = [NSDate:QueryAndStoreStepCountForDayAction]()
    private var dayStepSamplesQueryActionMap = [NSDate:QueryAndStoreStepCountSamplesForDayAction]()
    private var thisWeekStepStatsQueryAction: DetermineAndStoreRecommendedWalkWindowAction?
    
    public var onError: ( (NSError)->() )?
    
    private let actionQueue = NSOperationQueue()
    
    public init() {}
}

// MARK:- SynchronousStepDataSource
extension CoreMotionPedometerStepDataSource: SynchronousStepDataSource {
    public func stepCountForDayWithDate(date: NSDate) -> Int? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        var stepCount: Int?
        dispatch_sync(serialQueue) {
            if let stepCountForDay = self.stepCountForDayMap[date] {
                stepCount = stepCountForDay
            }
        }
        return stepCount
    }
    
    public func stepCountSamplesForDayWithDate(date: NSDate) -> StepCountSamples? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        var samples: StepCountSamples?
        dispatch_sync(serialQueue) {
            if let stepCountSamplesForDay = self.stepCountSamplesForDayMap[date] {
                samples = stepCountSamplesForDay
            }
        }
        return samples
    }
    
    public func stepCountStatsForThisWeek() -> StepCountStats? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        var recommendedWalkTimeResultOrNil: ActivityTimeWindowResult?
        dispatch_sync(serialQueue) {
            recommendedWalkTimeResultOrNil = self.recommendedWalkWindow
        }
        if let result = recommendedWalkTimeResultOrNil {
            switch result {
            case .Success(let window):
                return StepCountStats(from: window.from, to: window.to)
            case .Failure:
                return StepCountStats(from: ActivityLevelSnapshot(endDateHour: -1, endDateMinute: -1, level: .Unknown),
                                      to: ActivityLevelSnapshot(endDateHour: -1, endDateMinute: -1, level: .Unknown))
            }
        }
        return nil
    }
}

// MARK:- AsynchronousStepDataSource
extension CoreMotionPedometerStepDataSource: AsynchronousStepDataSource {
    public func scheduleGetStepCountForDayActionWithDate(action: GetStepCountForDayAction) {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        let queryAction = dayStepCountQueryAndStoreActionForDate(action.date)
        action.addDependency(queryAction)
        actionQueue.addOperation(action)
    }
    
    public func scheduleGetStepCountSamplesForDayActionWithDate(action: GetStepCountSamplesForDayAction) {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        let queryAction = dayStepSamplesQueryAndStoreActionForDate(action.date)
        action.addDependency(queryAction)
        actionQueue.addOperation(action)
    }
    
    public func scheduleGetStepCountStatsForThisWeekAction(action: GetStepCountStatsForThisWeekAction) {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        let determineAndStoreAction = determineAndStoreRecommendationWalkWindowAction()
        action.addDependency(determineAndStoreAction)
        actionQueue.addOperation(action)
    }
}

// MARK:- Private Methods
extension CoreMotionPedometerStepDataSource {
    private func dayStepCountQueryAndStoreActionForDate(date: NSDate) -> QueryAndStoreStepCountForDayAction {
        if let action = dayStepCountQueryActionMap[date] {
            return action
        } else {
            let action = QueryAndStoreStepCountForDayAction(date: date, dataSource: self)
            action.completionBlock = { [weak self, weak action] in
                if let strongSelf = self, let strongAction = action, let error = strongAction.errorOrNil {
                    strongSelf.onError?(error)
                }
            }
            dayStepCountQueryActionMap[date] = action
            actionQueue.addOperation(action)
            return action
        }
    }
    
    private func dayStepSamplesQueryAndStoreActionForDate(date: NSDate) -> QueryAndStoreStepCountSamplesForDayAction {
        if let action = dayStepSamplesQueryActionMap[date] {
            return action
        } else {
            let action = QueryAndStoreStepCountSamplesForDayAction(date: date, dataSource: self)
            action.completionBlock = { [weak self, weak action] in
                if let strongSelf = self, let strongAction = action, let error = strongAction.errorOrNil {
                    strongSelf.onError?(error)
                }
            }
            dayStepSamplesQueryActionMap[date] = action
            actionQueue.addOperation(action)
            return action
        }
    }
    
    private func determineAndStoreRecommendationWalkWindowAction() -> DetermineAndStoreRecommendedWalkWindowAction {
        if let determineAndStoreAction = thisWeekStepStatsQueryAction {
            return determineAndStoreAction
        } else {
            // Kick off samples action for each day of week in order to cross reference
            let dates = WeekDateFactory.datesForThisWeek()
            var actions = [QueryAndStoreStepCountSamplesForDayAction]()
            for date in dates {
                let queryAction = dayStepSamplesQueryAndStoreActionForDate(date)
                actions.append(queryAction)
            }
            // Create determine and store recommended walk window action
            let action = DetermineAndStoreRecommendedWalkWindowAction(sampleQueryAndStoreActions: actions, dataSource: self)
            thisWeekStepStatsQueryAction = action
            action.completionBlock = { [weak self, weak action] in
                if let strongSelf = self, let strongAction = action, let error = strongAction.errorOrNil {
                    strongSelf.onError?(error)
                }
            }
            actionQueue.addOperation(action)
            return action
        }
    }
}


private class QueryAndStoreStepCountForDayAction: AsyncAction {
    weak var dataSource: CoreMotionPedometerStepDataSource?
    let date: NSDate
    var queryAction: QueryPedometerAction?
    var errorOrNil: NSError?
    
    init(date: NSDate, dataSource: CoreMotionPedometerStepDataSource) {
        self.date = date
        self.dataSource = dataSource
        super.init()
    }
    
    override func run() {
        queryAction = QueryPedometerAction(startDate: date, endDate: date.dateByAddingTwentyFourHours()) {
            [weak self] result in
            if let strongSelf = self {
                if let strongDataSource = strongSelf.dataSource {
                    switch result {
                    case .Success(let data):
                        dispatch_async(strongDataSource.serialQueue) {
                            strongDataSource.stepCountForDayMap[data.startDate] = data.numberOfSteps.integerValue
                            return
                        }
                    case .Failure(let errorOrNil):
                        strongSelf.errorOrNil = errorOrNil
                    }
                }
                strongSelf.finishExecutingOperation()
            }
        }
        queryAction?.start()
    }
}

private class QueryAndStoreStepCountSamplesForDayAction: AsyncAction {
    weak var dataSource: CoreMotionPedometerStepDataSource?
    let date: NSDate
    var queryAction: QueryPedometerOneDayAction?
    var errorOrNil: NSError?
    
    init(date: NSDate, dataSource: CoreMotionPedometerStepDataSource) {
        self.date = date
        self.dataSource = dataSource
        super.init()
    }
    
    override func run() {
        queryAction = QueryPedometerOneDayAction(startDate: date) { [weak self] result in
            if let strongSelf = self {
                switch result {
                case .Success(let samples):
                    if let strongDataSource = strongSelf.dataSource {
                        dispatch_async(strongDataSource.serialQueue) {
                            strongDataSource.stepCountSamplesForDayMap[strongSelf.date] = samples
                            return
                        }
                    }
                case .Failure(let errorOrNil):
                    strongSelf.errorOrNil = errorOrNil
                }
                strongSelf.finishExecutingOperation()
            }
        }
        queryAction?.start()
    }
}

private class DetermineAndStoreRecommendedWalkWindowAction: Action {
    weak var dataSource: CoreMotionPedometerStepDataSource?
    let sampleQueryAndStoreActions: [QueryAndStoreStepCountSamplesForDayAction]
    var errorOrNil: NSError?
    
    init(sampleQueryAndStoreActions: [QueryAndStoreStepCountSamplesForDayAction], dataSource:CoreMotionPedometerStepDataSource) {
        self.sampleQueryAndStoreActions = sampleQueryAndStoreActions
        self.dataSource = dataSource
        super.init()
        for action in self.sampleQueryAndStoreActions {
            addDependency(action)
        }
    }
    
    private override func run() {
        if let strongDataSource = dataSource {
            var thisWeeksSamples = [StepCountSamples]()
            for action in sampleQueryAndStoreActions {
                dispatch_sync(dispatch_get_main_queue()) {
                    if let samples = strongDataSource.stepCountSamplesForDayWithDate(action.date) {
                        thisWeeksSamples.append(samples)
                    }
                }
            }
            let recommendationAction = DetermineWalkTimeRecommendationAction(thisWeeksSamples: thisWeeksSamples)
            recommendationAction.start()
            switch recommendationAction.result {
            case .Success:
                break
            case .Failure(let errorOrNil):
                self.errorOrNil = errorOrNil
            }
            dispatch_sync(strongDataSource.serialQueue) {
                strongDataSource.recommendedWalkWindow = recommendationAction.result
                
            }
        }
    }
}
