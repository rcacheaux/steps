//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation
import CoreMotion

public class QueryPedometerAction: AsyncAction {
    // Input
    private let dateInterval: (NSDate, NSDate)
    private let onComplete: ( (PedometerDataResult)->() )?
    // Output
    var result: PedometerDataResult?
    // Operation specific state
    private let pedometer = CMPedometer()
    
    convenience init(startDate: NSDate, endDate: NSDate) {
        self.init(startDate: startDate, endDate: endDate, onComplete: nil)
    }
    
    init(startDate: NSDate, endDate: NSDate, onComplete: ( (PedometerDataResult)->() )?) {
        self.dateInterval = (startDate, endDate)
        self.onComplete = onComplete
        super.init()
    }
    
    public override func run() {
        pedometer.queryPedometerDataFromDate(dateInterval.0, toDate: dateInterval.1) {
            [weak self] dataOrNil, errorOrNil in
            if let strongSelf = self {
                switch (dataOrNil, errorOrNil) {
                case (let data?, _):
                    strongSelf.result = .Success(data)
                default:
                    if let error = errorOrNil {
                        if error.code == CMErrorMotionActivityNotAuthorized.rawIntValue {
                            strongSelf.result = .Failure(NSError.unauthorizedStepQueryError())
                            break
                        }
                    }
                    strongSelf.result = .Failure(errorOrNil)
                }
                strongSelf.finishExecutingOperation()
            }
        }
    }
    
    public override func actionCompleted() {
        assert(result != nil)
        onComplete?(result!)
    }
}
