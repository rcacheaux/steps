//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation
import CoreMotion

public class QueryPedometerOneDayAction: AsyncAction {
    // Input
    private let startDate: NSDate
    private let onComplete: ( (StepCountSamplesResult)->() )?
    // Output
    public private(set) var intervalEndDates = [NSDate]()
    public private(set) var stepCountMap = [NSDate:(count: Int, level: ActivityLevel)]()
    public private(set) var activityLevelSnapshotSetMap = ActivityLevelSnapshotSetMap()
    var errorOrNil: NSError?
    // Operation specific state
    private let queryOperationQueue = NSOperationQueue()
    
    public convenience init(startDate: NSDate) {
        self.init(startDate: startDate, onComplete: nil)
    }
    
    init(startDate: NSDate, onComplete: ( (StepCountSamplesResult)->() )?) {
        self.startDate = startDate
        self.onComplete = onComplete
        super.init()
    }
    
    public override func run() {
        var startDate = self.startDate
        var queryActions = [QueryPedometerAction]()
        
        for _ in 1...48 {
            let endDate = startDate.dateByAddingTimeInterval(30 * 60)
            intervalEndDates.append(endDate)
            
            let action = QueryPedometerAction(startDate: startDate, endDate: endDate)
            queryOperationQueue.addOperation(action)
            queryActions.append(action)
            
            startDate = endDate
        }
        
        let resultAction = QueryPedometerOneDayResultAction(queryActions: queryActions)
        resultAction.completionBlock = { [weak resultAction, weak self] in
            if let strongSelf = self, let strongAction = resultAction {
                strongSelf.activityLevelSnapshotSetMap = strongAction.activityLevelSnapshotSetMap
                strongSelf.stepCountMap = strongAction.stepCountMap
                strongSelf.finishExecutingOperation()
                strongSelf.errorOrNil = strongAction.errorOrNil
            }
        }
        queryOperationQueue.addOperation(resultAction)
    }
    
    public override func actionCompleted() {
        let samples = StepCountSamples(intervalEndDates: intervalEndDates,
                                       stepCountMap: stepCountMap, activityLevelSnapshotSetMap: activityLevelSnapshotSetMap)
        if let error = errorOrNil {
            onComplete?(.Failure(error))
        } else {
            onComplete?(.Success(samples))
        }
    }
}

private class QueryPedometerOneDayResultAction: Action {
    let queryActions: [QueryPedometerAction]
    var stepCountMap = [NSDate:(count: Int, level: ActivityLevel)]()
    var activityLevelSnapshotSetMap: ActivityLevelSnapshotSetMap = {
        var map = ActivityLevelSnapshotSetMap()
        for level in ActivityLevel.allValues {
            map[level] = Set<ActivityLevelSnapshot>(minimumCapacity: 48)
        }
        return map
        }()
    var errorOrNil: NSError?
    
    init(queryActions: [QueryPedometerAction]) {
        self.queryActions = queryActions
        super.init()
        for action in self.queryActions {
            addDependency(action)
        }
    }
    
    override func run() {
        for action in queryActions {
            switch action.result! {
            case .Success(let data):
                // Store activity level
                let level = ActivityLevel(numberOfStepsWithinThirtyMinutes: data.numberOfSteps.integerValue)
                let snapshot = ActivityLevelSnapshot(endDate: data.endDate, level:level)
                activityLevelSnapshotSetMap[level]?.insert(snapshot)
                // Store step count
                stepCountMap[data.endDate] = (count: data.numberOfSteps.integerValue, level: level)
            case .Failure(let errorOrNil):
                switch errorOrNil {
                case let error? where error.code == CMErrorMotionActivityNotAuthorized.rawIntValue:
                    self.errorOrNil = NSError.unauthorizedStepQueryError()
                case _ where self.errorOrNil == nil:
                    self.errorOrNil = NSError.coreMotionPedometerQueryError()
                default:
                    break;
                }
            }
        }
    }
}

extension CMError {
    var rawIntValue: Int {
        return Int(rawValue)
    }
}
