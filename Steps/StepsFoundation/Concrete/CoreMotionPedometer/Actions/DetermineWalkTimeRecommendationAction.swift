//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

class DetermineWalkTimeRecommendationAction: Action {
    let thisWeeksSamples: [StepCountSamples]
    var result: ActivityTimeWindowResult = .Failure(NSError.walkTimeRecommendationError())
    
    init(thisWeeksSamples: [StepCountSamples]) {
        self.thisWeeksSamples = thisWeeksSamples
        super.init()
    }
    
    override func run() {
        if thisWeeksSamples.count == 0 { return }
        
        let activitySet = intersectedSetForOverlappingActivityLevel(thisWeeksSamples, level: .Underactive)
        if activitySet.count == 0 { return }
        
        let sortedUnderactiveSnapshots = activitySet.sort()
        let underActiveWindows = findContiguousActivitySnapshots(sortedUnderactiveSnapshots)
        result = .Success(findLongestNonSleepTimeWindow(underActiveWindows))
    }
    
    func intersectedSetForOverlappingActivityLevel(allSamples: [StepCountSamples],
                                                   level: ActivityLevel) -> Set<ActivityLevelSnapshot> {
        assert(allSamples.count > 0)
        var activityLevelSet = allSamples.first!.activityLevelSnapshotSetMap[level]!
        for index in 1..<allSamples.count {
            activityLevelSet.intersectInPlace(allSamples[index].activityLevelSnapshotSetMap[level]!)
        }
        return activityLevelSet
    }
    
    func findContiguousActivitySnapshots(sortedSnapshots: [ActivityLevelSnapshot]) -> [ActivityTimeWindow] {
        var overlappingWindows = [(from: sortedSnapshots[0], to: sortedSnapshots[0])]
        for index in 1..<sortedSnapshots.count {
            let snapshot = sortedSnapshots[index]
            var lastWindow = overlappingWindows.last!
            if lastWindow.to.endDateHour == snapshot.endDateHour {
                // Same hour, these two snapshots are next to each other, extend the window
                lastWindow.to = snapshot
                overlappingWindows[overlappingWindows.endIndex-1] = lastWindow
            } else if lastWindow.to.endDateHour + 1 == snapshot.endDateHour && snapshot.endDateMinute == 0 {
                // These two snapshots are next to each other, extend the window
                lastWindow.to = snapshot
                overlappingWindows[overlappingWindows.endIndex-1] = lastWindow
            } else {
                // Create new time window, these snapshots are not next to each other
                overlappingWindows.append((from: snapshot, to: snapshot))
            }
        }
        return overlappingWindows
    }
    
    func findLongestNonSleepTimeWindow(windows: [ActivityTimeWindow]) -> ActivityTimeWindow {
        assert(windows.count > 0)
        var longestNonSleepWindow = windows.first!
        var largestDelta = -1.0
        let morningSleepHourThreshold = 4 // Don't consider windows starting before 4 AM
        let eveningSleepHourThreshold = 19 // Don't consider windows starting after 7 PM
        for window in windows {
            let delta = window.to.numericValue - window.from.numericValue
            if delta > largestDelta && window.from.endDateHour > morningSleepHourThreshold &&
                                       window.from.endDateHour < eveningSleepHourThreshold {
                longestNonSleepWindow = window
                largestDelta = delta
            }
        }
        return longestNonSleepWindow
    }
}
