//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

// TODO: Place fake data into file and pull from disk. I didn't have time to do this, but I would normally place
//   this kind of data inside a file perhaps encoded by NSCoding to be able to pull quickly into memory. Placing
//   all the data in a .swift file like this slows down the compiler significantly.

#if arch(x86_64) || arch(i386)
let fakeWeekDates = [
    NSDate(timeIntervalSince1970: 1433566800.0),
    NSDate(timeIntervalSince1970: 1433653200.0),
    NSDate(timeIntervalSince1970: 1433739600.0),
    NSDate(timeIntervalSince1970: 1433826000.0),
    NSDate(timeIntervalSince1970: 1433912400.0),
    NSDate(timeIntervalSince1970: 1433998800.0),
    NSDate(timeIntervalSince1970: 1434085200.0)]
let endDatesMap = [NSDate(timeIntervalSince1970: 1433566800.0): [
    NSDate(timeIntervalSince1970: 1433568600.0),
    NSDate(timeIntervalSince1970: 1433570400.0),
    NSDate(timeIntervalSince1970: 1433572200.0),
    NSDate(timeIntervalSince1970: 1433574000.0),
    NSDate(timeIntervalSince1970: 1433575800.0),
    NSDate(timeIntervalSince1970: 1433577600.0),
    NSDate(timeIntervalSince1970: 1433579400.0),
    NSDate(timeIntervalSince1970: 1433581200.0),
    NSDate(timeIntervalSince1970: 1433583000.0),
    NSDate(timeIntervalSince1970: 1433584800.0),
    NSDate(timeIntervalSince1970: 1433586600.0),
    NSDate(timeIntervalSince1970: 1433588400.0),
    NSDate(timeIntervalSince1970: 1433590200.0),
    NSDate(timeIntervalSince1970: 1433592000.0),
    NSDate(timeIntervalSince1970: 1433593800.0),
    NSDate(timeIntervalSince1970: 1433595600.0),
    NSDate(timeIntervalSince1970: 1433597400.0),
    NSDate(timeIntervalSince1970: 1433599200.0),
    NSDate(timeIntervalSince1970: 1433601000.0),
    NSDate(timeIntervalSince1970: 1433602800.0),
    NSDate(timeIntervalSince1970: 1433604600.0),
    NSDate(timeIntervalSince1970: 1433606400.0),
    NSDate(timeIntervalSince1970: 1433608200.0),
    NSDate(timeIntervalSince1970: 1433610000.0),
    NSDate(timeIntervalSince1970: 1433611800.0),
    NSDate(timeIntervalSince1970: 1433613600.0),
    NSDate(timeIntervalSince1970: 1433615400.0),
    NSDate(timeIntervalSince1970: 1433617200.0),
    NSDate(timeIntervalSince1970: 1433619000.0),
    NSDate(timeIntervalSince1970: 1433620800.0),
    NSDate(timeIntervalSince1970: 1433622600.0),
    NSDate(timeIntervalSince1970: 1433624400.0),
    NSDate(timeIntervalSince1970: 1433626200.0),
    NSDate(timeIntervalSince1970: 1433628000.0),
    NSDate(timeIntervalSince1970: 1433629800.0),
    NSDate(timeIntervalSince1970: 1433631600.0),
    NSDate(timeIntervalSince1970: 1433633400.0),
    NSDate(timeIntervalSince1970: 1433635200.0),
    NSDate(timeIntervalSince1970: 1433637000.0),
    NSDate(timeIntervalSince1970: 1433638800.0),
    NSDate(timeIntervalSince1970: 1433640600.0),
    NSDate(timeIntervalSince1970: 1433642400.0),
    NSDate(timeIntervalSince1970: 1433644200.0),
    NSDate(timeIntervalSince1970: 1433646000.0),
    NSDate(timeIntervalSince1970: 1433647800.0),
    NSDate(timeIntervalSince1970: 1433649600.0),
    NSDate(timeIntervalSince1970: 1433651400.0),
    NSDate(timeIntervalSince1970: 1433653200.0)],
NSDate(timeIntervalSince1970: 1433653200.0): [
    NSDate(timeIntervalSince1970: 1433655000.0),
    NSDate(timeIntervalSince1970: 1433656800.0),
    NSDate(timeIntervalSince1970: 1433658600.0),
    NSDate(timeIntervalSince1970: 1433660400.0),
    NSDate(timeIntervalSince1970: 1433662200.0),
    NSDate(timeIntervalSince1970: 1433664000.0),
    NSDate(timeIntervalSince1970: 1433665800.0),
    NSDate(timeIntervalSince1970: 1433667600.0),
    NSDate(timeIntervalSince1970: 1433669400.0),
    NSDate(timeIntervalSince1970: 1433671200.0),
    NSDate(timeIntervalSince1970: 1433673000.0),
    NSDate(timeIntervalSince1970: 1433674800.0),
    NSDate(timeIntervalSince1970: 1433676600.0),
    NSDate(timeIntervalSince1970: 1433678400.0),
    NSDate(timeIntervalSince1970: 1433680200.0),
    NSDate(timeIntervalSince1970: 1433682000.0),
    NSDate(timeIntervalSince1970: 1433683800.0),
    NSDate(timeIntervalSince1970: 1433685600.0),
    NSDate(timeIntervalSince1970: 1433687400.0),
    NSDate(timeIntervalSince1970: 1433689200.0),
    NSDate(timeIntervalSince1970: 1433691000.0),
    NSDate(timeIntervalSince1970: 1433692800.0),
    NSDate(timeIntervalSince1970: 1433694600.0),
    NSDate(timeIntervalSince1970: 1433696400.0),
    NSDate(timeIntervalSince1970: 1433698200.0),
    NSDate(timeIntervalSince1970: 1433700000.0),
    NSDate(timeIntervalSince1970: 1433701800.0),
    NSDate(timeIntervalSince1970: 1433703600.0),
    NSDate(timeIntervalSince1970: 1433705400.0),
    NSDate(timeIntervalSince1970: 1433707200.0),
    NSDate(timeIntervalSince1970: 1433709000.0),
    NSDate(timeIntervalSince1970: 1433710800.0),
    NSDate(timeIntervalSince1970: 1433712600.0),
    NSDate(timeIntervalSince1970: 1433714400.0),
    NSDate(timeIntervalSince1970: 1433716200.0),
    NSDate(timeIntervalSince1970: 1433718000.0),
    NSDate(timeIntervalSince1970: 1433719800.0),
    NSDate(timeIntervalSince1970: 1433721600.0),
    NSDate(timeIntervalSince1970: 1433723400.0),
    NSDate(timeIntervalSince1970: 1433725200.0),
    NSDate(timeIntervalSince1970: 1433727000.0),
    NSDate(timeIntervalSince1970: 1433728800.0),
    NSDate(timeIntervalSince1970: 1433730600.0),
    NSDate(timeIntervalSince1970: 1433732400.0),
    NSDate(timeIntervalSince1970: 1433734200.0),
    NSDate(timeIntervalSince1970: 1433736000.0),
    NSDate(timeIntervalSince1970: 1433737800.0),
    NSDate(timeIntervalSince1970: 1433739600.0)],
NSDate(timeIntervalSince1970: 1433739600.0): [
    NSDate(timeIntervalSince1970: 1433741400.0),
    NSDate(timeIntervalSince1970: 1433743200.0),
    NSDate(timeIntervalSince1970: 1433745000.0),
    NSDate(timeIntervalSince1970: 1433746800.0),
    NSDate(timeIntervalSince1970: 1433748600.0),
    NSDate(timeIntervalSince1970: 1433750400.0),
    NSDate(timeIntervalSince1970: 1433752200.0),
    NSDate(timeIntervalSince1970: 1433754000.0),
    NSDate(timeIntervalSince1970: 1433755800.0),
    NSDate(timeIntervalSince1970: 1433757600.0),
    NSDate(timeIntervalSince1970: 1433759400.0),
    NSDate(timeIntervalSince1970: 1433761200.0),
    NSDate(timeIntervalSince1970: 1433763000.0),
    NSDate(timeIntervalSince1970: 1433764800.0),
    NSDate(timeIntervalSince1970: 1433766600.0),
    NSDate(timeIntervalSince1970: 1433768400.0),
    NSDate(timeIntervalSince1970: 1433770200.0),
    NSDate(timeIntervalSince1970: 1433772000.0),
    NSDate(timeIntervalSince1970: 1433773800.0),
    NSDate(timeIntervalSince1970: 1433775600.0),
    NSDate(timeIntervalSince1970: 1433777400.0),
    NSDate(timeIntervalSince1970: 1433779200.0),
    NSDate(timeIntervalSince1970: 1433781000.0),
    NSDate(timeIntervalSince1970: 1433782800.0),
    NSDate(timeIntervalSince1970: 1433784600.0),
    NSDate(timeIntervalSince1970: 1433786400.0),
    NSDate(timeIntervalSince1970: 1433788200.0),
    NSDate(timeIntervalSince1970: 1433790000.0),
    NSDate(timeIntervalSince1970: 1433791800.0),
    NSDate(timeIntervalSince1970: 1433793600.0),
    NSDate(timeIntervalSince1970: 1433795400.0),
    NSDate(timeIntervalSince1970: 1433797200.0),
    NSDate(timeIntervalSince1970: 1433799000.0),
    NSDate(timeIntervalSince1970: 1433800800.0),
    NSDate(timeIntervalSince1970: 1433802600.0),
    NSDate(timeIntervalSince1970: 1433804400.0),
    NSDate(timeIntervalSince1970: 1433806200.0),
    NSDate(timeIntervalSince1970: 1433808000.0),
    NSDate(timeIntervalSince1970: 1433809800.0),
    NSDate(timeIntervalSince1970: 1433811600.0),
    NSDate(timeIntervalSince1970: 1433813400.0),
    NSDate(timeIntervalSince1970: 1433815200.0),
    NSDate(timeIntervalSince1970: 1433817000.0),
    NSDate(timeIntervalSince1970: 1433818800.0),
    NSDate(timeIntervalSince1970: 1433820600.0),
    NSDate(timeIntervalSince1970: 1433822400.0),
    NSDate(timeIntervalSince1970: 1433824200.0),
    NSDate(timeIntervalSince1970: 1433826000.0)],
NSDate(timeIntervalSince1970: 1433826000.0): [
    NSDate(timeIntervalSince1970: 1433827800.0),
    NSDate(timeIntervalSince1970: 1433829600.0),
    NSDate(timeIntervalSince1970: 1433831400.0),
    NSDate(timeIntervalSince1970: 1433833200.0),
    NSDate(timeIntervalSince1970: 1433835000.0),
    NSDate(timeIntervalSince1970: 1433836800.0),
    NSDate(timeIntervalSince1970: 1433838600.0),
    NSDate(timeIntervalSince1970: 1433840400.0),
    NSDate(timeIntervalSince1970: 1433842200.0),
    NSDate(timeIntervalSince1970: 1433844000.0),
    NSDate(timeIntervalSince1970: 1433845800.0),
    NSDate(timeIntervalSince1970: 1433847600.0),
    NSDate(timeIntervalSince1970: 1433849400.0),
    NSDate(timeIntervalSince1970: 1433851200.0),
    NSDate(timeIntervalSince1970: 1433853000.0),
    NSDate(timeIntervalSince1970: 1433854800.0),
    NSDate(timeIntervalSince1970: 1433856600.0),
    NSDate(timeIntervalSince1970: 1433858400.0),
    NSDate(timeIntervalSince1970: 1433860200.0),
    NSDate(timeIntervalSince1970: 1433862000.0),
    NSDate(timeIntervalSince1970: 1433863800.0),
    NSDate(timeIntervalSince1970: 1433865600.0),
    NSDate(timeIntervalSince1970: 1433867400.0),
    NSDate(timeIntervalSince1970: 1433869200.0),
    NSDate(timeIntervalSince1970: 1433871000.0),
    NSDate(timeIntervalSince1970: 1433872800.0),
    NSDate(timeIntervalSince1970: 1433874600.0),
    NSDate(timeIntervalSince1970: 1433876400.0),
    NSDate(timeIntervalSince1970: 1433878200.0),
    NSDate(timeIntervalSince1970: 1433880000.0),
    NSDate(timeIntervalSince1970: 1433881800.0),
    NSDate(timeIntervalSince1970: 1433883600.0),
    NSDate(timeIntervalSince1970: 1433885400.0),
    NSDate(timeIntervalSince1970: 1433887200.0),
    NSDate(timeIntervalSince1970: 1433889000.0),
    NSDate(timeIntervalSince1970: 1433890800.0),
    NSDate(timeIntervalSince1970: 1433892600.0),
    NSDate(timeIntervalSince1970: 1433894400.0),
    NSDate(timeIntervalSince1970: 1433896200.0),
    NSDate(timeIntervalSince1970: 1433898000.0),
    NSDate(timeIntervalSince1970: 1433899800.0),
    NSDate(timeIntervalSince1970: 1433901600.0),
    NSDate(timeIntervalSince1970: 1433903400.0),
    NSDate(timeIntervalSince1970: 1433905200.0),
    NSDate(timeIntervalSince1970: 1433907000.0),
    NSDate(timeIntervalSince1970: 1433908800.0),
    NSDate(timeIntervalSince1970: 1433910600.0),
    NSDate(timeIntervalSince1970: 1433912400.0)],
NSDate(timeIntervalSince1970: 1433912400.0): [
    NSDate(timeIntervalSince1970: 1433914200.0),
    NSDate(timeIntervalSince1970: 1433916000.0),
    NSDate(timeIntervalSince1970: 1433917800.0),
    NSDate(timeIntervalSince1970: 1433919600.0),
    NSDate(timeIntervalSince1970: 1433921400.0),
    NSDate(timeIntervalSince1970: 1433923200.0),
    NSDate(timeIntervalSince1970: 1433925000.0),
    NSDate(timeIntervalSince1970: 1433926800.0),
    NSDate(timeIntervalSince1970: 1433928600.0),
    NSDate(timeIntervalSince1970: 1433930400.0),
    NSDate(timeIntervalSince1970: 1433932200.0),
    NSDate(timeIntervalSince1970: 1433934000.0),
    NSDate(timeIntervalSince1970: 1433935800.0),
    NSDate(timeIntervalSince1970: 1433937600.0),
    NSDate(timeIntervalSince1970: 1433939400.0),
    NSDate(timeIntervalSince1970: 1433941200.0),
    NSDate(timeIntervalSince1970: 1433943000.0),
    NSDate(timeIntervalSince1970: 1433944800.0),
    NSDate(timeIntervalSince1970: 1433946600.0),
    NSDate(timeIntervalSince1970: 1433948400.0),
    NSDate(timeIntervalSince1970: 1433950200.0),
    NSDate(timeIntervalSince1970: 1433952000.0),
    NSDate(timeIntervalSince1970: 1433953800.0),
    NSDate(timeIntervalSince1970: 1433955600.0),
    NSDate(timeIntervalSince1970: 1433957400.0),
    NSDate(timeIntervalSince1970: 1433959200.0),
    NSDate(timeIntervalSince1970: 1433961000.0),
    NSDate(timeIntervalSince1970: 1433962800.0),
    NSDate(timeIntervalSince1970: 1433964600.0),
    NSDate(timeIntervalSince1970: 1433966400.0),
    NSDate(timeIntervalSince1970: 1433968200.0),
    NSDate(timeIntervalSince1970: 1433970000.0),
    NSDate(timeIntervalSince1970: 1433971800.0),
    NSDate(timeIntervalSince1970: 1433973600.0),
    NSDate(timeIntervalSince1970: 1433975400.0),
    NSDate(timeIntervalSince1970: 1433977200.0),
    NSDate(timeIntervalSince1970: 1433979000.0),
    NSDate(timeIntervalSince1970: 1433980800.0),
    NSDate(timeIntervalSince1970: 1433982600.0),
    NSDate(timeIntervalSince1970: 1433984400.0),
    NSDate(timeIntervalSince1970: 1433986200.0),
    NSDate(timeIntervalSince1970: 1433988000.0),
    NSDate(timeIntervalSince1970: 1433989800.0),
    NSDate(timeIntervalSince1970: 1433991600.0),
    NSDate(timeIntervalSince1970: 1433993400.0),
    NSDate(timeIntervalSince1970: 1433995200.0),
    NSDate(timeIntervalSince1970: 1433997000.0),
    NSDate(timeIntervalSince1970: 1433998800.0)],
NSDate(timeIntervalSince1970: 1433998800.0): [
    NSDate(timeIntervalSince1970: 1434000600.0),
    NSDate(timeIntervalSince1970: 1434002400.0),
    NSDate(timeIntervalSince1970: 1434004200.0),
    NSDate(timeIntervalSince1970: 1434006000.0),
    NSDate(timeIntervalSince1970: 1434007800.0),
    NSDate(timeIntervalSince1970: 1434009600.0),
    NSDate(timeIntervalSince1970: 1434011400.0),
    NSDate(timeIntervalSince1970: 1434013200.0),
    NSDate(timeIntervalSince1970: 1434015000.0),
    NSDate(timeIntervalSince1970: 1434016800.0),
    NSDate(timeIntervalSince1970: 1434018600.0),
    NSDate(timeIntervalSince1970: 1434020400.0),
    NSDate(timeIntervalSince1970: 1434022200.0),
    NSDate(timeIntervalSince1970: 1434024000.0),
    NSDate(timeIntervalSince1970: 1434025800.0),
    NSDate(timeIntervalSince1970: 1434027600.0),
    NSDate(timeIntervalSince1970: 1434029400.0),
    NSDate(timeIntervalSince1970: 1434031200.0),
    NSDate(timeIntervalSince1970: 1434033000.0),
    NSDate(timeIntervalSince1970: 1434034800.0),
    NSDate(timeIntervalSince1970: 1434036600.0),
    NSDate(timeIntervalSince1970: 1434038400.0),
    NSDate(timeIntervalSince1970: 1434040200.0),
    NSDate(timeIntervalSince1970: 1434042000.0),
    NSDate(timeIntervalSince1970: 1434043800.0),
    NSDate(timeIntervalSince1970: 1434045600.0),
    NSDate(timeIntervalSince1970: 1434047400.0),
    NSDate(timeIntervalSince1970: 1434049200.0),
    NSDate(timeIntervalSince1970: 1434051000.0),
    NSDate(timeIntervalSince1970: 1434052800.0),
    NSDate(timeIntervalSince1970: 1434054600.0),
    NSDate(timeIntervalSince1970: 1434056400.0),
    NSDate(timeIntervalSince1970: 1434058200.0),
    NSDate(timeIntervalSince1970: 1434060000.0),
    NSDate(timeIntervalSince1970: 1434061800.0),
    NSDate(timeIntervalSince1970: 1434063600.0),
    NSDate(timeIntervalSince1970: 1434065400.0),
    NSDate(timeIntervalSince1970: 1434067200.0),
    NSDate(timeIntervalSince1970: 1434069000.0),
    NSDate(timeIntervalSince1970: 1434070800.0),
    NSDate(timeIntervalSince1970: 1434072600.0),
    NSDate(timeIntervalSince1970: 1434074400.0),
    NSDate(timeIntervalSince1970: 1434076200.0),
    NSDate(timeIntervalSince1970: 1434078000.0),
    NSDate(timeIntervalSince1970: 1434079800.0),
    NSDate(timeIntervalSince1970: 1434081600.0),
    NSDate(timeIntervalSince1970: 1434083400.0),
    NSDate(timeIntervalSince1970: 1434085200.0)],
NSDate(timeIntervalSince1970: 1434085200.0): [
    NSDate(timeIntervalSince1970: 1434087000.0),
    NSDate(timeIntervalSince1970: 1434088800.0),
    NSDate(timeIntervalSince1970: 1434090600.0),
    NSDate(timeIntervalSince1970: 1434092400.0),
    NSDate(timeIntervalSince1970: 1434094200.0),
    NSDate(timeIntervalSince1970: 1434096000.0),
    NSDate(timeIntervalSince1970: 1434097800.0),
    NSDate(timeIntervalSince1970: 1434099600.0),
    NSDate(timeIntervalSince1970: 1434101400.0),
    NSDate(timeIntervalSince1970: 1434103200.0),
    NSDate(timeIntervalSince1970: 1434105000.0),
    NSDate(timeIntervalSince1970: 1434106800.0),
    NSDate(timeIntervalSince1970: 1434108600.0),
    NSDate(timeIntervalSince1970: 1434110400.0),
    NSDate(timeIntervalSince1970: 1434112200.0),
    NSDate(timeIntervalSince1970: 1434114000.0),
    NSDate(timeIntervalSince1970: 1434115800.0),
    NSDate(timeIntervalSince1970: 1434117600.0),
    NSDate(timeIntervalSince1970: 1434119400.0),
    NSDate(timeIntervalSince1970: 1434121200.0),
    NSDate(timeIntervalSince1970: 1434123000.0),
    NSDate(timeIntervalSince1970: 1434124800.0),
    NSDate(timeIntervalSince1970: 1434126600.0),
    NSDate(timeIntervalSince1970: 1434128400.0),
    NSDate(timeIntervalSince1970: 1434130200.0),
    NSDate(timeIntervalSince1970: 1434132000.0),
    NSDate(timeIntervalSince1970: 1434133800.0),
    NSDate(timeIntervalSince1970: 1434135600.0),
    NSDate(timeIntervalSince1970: 1434137400.0),
    NSDate(timeIntervalSince1970: 1434139200.0),
    NSDate(timeIntervalSince1970: 1434141000.0),
    NSDate(timeIntervalSince1970: 1434142800.0),
    NSDate(timeIntervalSince1970: 1434144600.0),
    NSDate(timeIntervalSince1970: 1434146400.0),
    NSDate(timeIntervalSince1970: 1434148200.0),
    NSDate(timeIntervalSince1970: 1434150000.0),
    NSDate(timeIntervalSince1970: 1434151800.0),
    NSDate(timeIntervalSince1970: 1434153600.0),
    NSDate(timeIntervalSince1970: 1434155400.0),
    NSDate(timeIntervalSince1970: 1434157200.0),
    NSDate(timeIntervalSince1970: 1434159000.0),
    NSDate(timeIntervalSince1970: 1434160800.0),
    NSDate(timeIntervalSince1970: 1434162600.0),
    NSDate(timeIntervalSince1970: 1434164400.0),
    NSDate(timeIntervalSince1970: 1434166200.0),
    NSDate(timeIntervalSince1970: 1434168000.0),
    NSDate(timeIntervalSince1970: 1434169800.0),
    NSDate(timeIntervalSince1970: 1434171600.0)]]
#endif
