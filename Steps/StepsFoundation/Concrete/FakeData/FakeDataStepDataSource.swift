//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

#if arch(x86_64) || arch(i386)
public class FakeDataStepDataSource: SynchronousStepDataSource, AsynchronousStepDataSource {
    public var onError: ( (NSError)->() )?
    let operationQueue = NSOperationQueue()
    
    public init() { }
    
    public func stepCountForDayWithDate(date: NSDate) -> Int? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        return dailyStepCountMap[date]!
    }
    
    public func stepCountSamplesForDayWithDate(date: NSDate) -> StepCountSamples? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        let intervalEndDates = endDatesMap[date]!
        let stepCountMap = stepMap[date]!
        return StepCountSamples(intervalEndDates: intervalEndDates, stepCountMap: stepCountMap, activityLevelSnapshotSetMap: [:])
    }
    
    public func stepCountStatsForThisWeek() -> StepCountStats? {
        guard NSThread.isMainThread() else { fatalError(mainThreadAffinityErrorString) }
        // TODO: Generate Activity Level Sets from fake data and run DetermineWalkTimeRecommendationAction
        //   so the simulator can run the recommendation algorithm. I planned on doing this but ran out of time.
        let from = ActivityLevelSnapshot(endDateHour: 8, endDateMinute: 30, level: .Underactive)
        let to = ActivityLevelSnapshot(endDateHour: 10, endDateMinute: 30, level: .Underactive)
        return StepCountStats(from: from, to: to)
    }
    
    public func scheduleGetStepCountForDayActionWithDate(action: GetStepCountForDayAction) {
        operationQueue.addOperation(action)
    }
    
    public func scheduleGetStepCountSamplesForDayActionWithDate(action: GetStepCountSamplesForDayAction) {
        operationQueue.addOperation(action)
    }
    
    public func scheduleGetStepCountStatsForThisWeekAction(action: GetStepCountStatsForThisWeekAction) {
        operationQueue.addOperation(action)
    }
}
#endif
