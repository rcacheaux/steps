//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation
import StepsFoundation

public class WeekDateFactory {
    public static func datesForThisWeek() -> [NSDate] {
        #if arch(x86_64) || arch(i386)
            return fakeWeekDates
        #else
            var date = NSDate.dateAtMidnightSixDaysAgo()
            var datesThisWeek = [date]
            for _ in 1...6 {
                date = date.dateByAddingTwentyFourHours()
                datesThisWeek.append(date)
            }
            return datesThisWeek
        #endif
    }
}
