//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

public enum Result<T> {
    case Success(T)
    case Failure(NSError?)
}
