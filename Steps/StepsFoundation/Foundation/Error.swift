//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

let mainThreadAffinityErrorString = "Must call data source on main queue."

extension NSError {
    class func defaultError() -> NSError {
        return NSError(.Default, "Unknown error occured.")
    }
    
    class func unauthorizedStepQueryError() -> NSError {
        return NSError(.Unauthorized, "This app does not have permission to access your fitness information. Please grant access within the Privacy section in the Settings app.")
    }
    
    class func coreMotionPedometerQueryError() -> NSError {
        return NSError(.CoreMotionPedometerQueryError, "Error occured retrieving fitness information, some step data will be missing.")
    }
    
    class func walkTimeRecommendationError() -> NSError {
        return NSError(.WalkRecommendationError, "Error occured, could not determine best walk time.")
    }
}

protocol Error {
    var code: Int { get }
    var domain: String { get }
}

enum StepQueryErrorCode: Int, Error, ErrorType {
    case Default
    case Unauthorized
    case CoreMotionPedometerQueryError
    case WalkRecommendationError
    
    var domain: String {
        return "com.underarmour.stepsfoundation.swift"
    }
    
    var code: Int {
        return self.rawValue
    }
}

extension NSError {
    convenience init(_ code: StepQueryErrorCode, _ localizedDescription: String) {
        self.init(domain: code.domain, code: code.code, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
    }
}
