//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

public typealias ActivityTimeWindow = (from: ActivityLevelSnapshot, to: ActivityLevelSnapshot)
public typealias ActivityLevelSnapshotSetMap = [ActivityLevel:Set<ActivityLevelSnapshot>]
public typealias ActivityTimeWindowResult = Result<ActivityTimeWindow>

typealias StepCountSamplesResult = Result<StepCountSamples>

public enum ActivityLevel: Int {
    case Unknown
    case Underactive
    case Active
    case HighlyActive
    
    public static var allValues: [ActivityLevel] {
        return [ActivityLevel.Unknown,
                ActivityLevel.Underactive,
                ActivityLevel.Active,
                ActivityLevel.HighlyActive]
    }
}

extension ActivityLevel {
    public init(numberOfStepsWithinThirtyMinutes: Int) {
        switch numberOfStepsWithinThirtyMinutes {
        case 0...499:
            self = .Underactive
        case 400...1999:
            self = .Active
        case let steps where steps >= 2000:
            self = .HighlyActive
        default:
            self = .Unknown
        }
    }
}

extension ActivityLevel: CustomStringConvertible {
    public var description: String {
        switch self {
        case .Unknown:
            return ""
        case .Underactive:
            return "Under Active"
        case .Active:
            return "Active"
        case .HighlyActive:
            return "Highly Active"
        }
    }
}

public struct ActivityLevelSnapshot {
    public let endDateHour: Int
    public let endDateMinute: Int
    let level: ActivityLevel
    
    var numericValue: Double {
        return Double(endDateHour) + Double(endDateMinute)/60.0
    }
    
    init(endDate: NSDate, level: ActivityLevel) {
        let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        let components = calendar.components([.Hour, .Minute], fromDate: endDate)
        self.init(endDateHour: components.hour, endDateMinute: components.minute, level: level)
    }
    
    init(endDateHour: Int, endDateMinute: Int, level: ActivityLevel) {
        self.endDateHour = endDateHour
        self.endDateMinute = endDateMinute
        self.level = level
    }
    
    public func componenetsBySubtractingThirtyMinutes() -> (hour: Int, minute: Int) {
        assert(endDateMinute == 0 || endDateMinute == 30)
        if endDateMinute == 0 {
            return (hour: endDateHour - 1, minute: endDateMinute + 30)
        } else {
            return (hour: endDateHour, minute: endDateMinute - 30)
        }
    }
}

extension ActivityLevelSnapshot: Hashable, Comparable {
    public var hashValue: Int {
        return endDateHour.hashValue ^ endDateMinute.hashValue ^ level.hashValue
    }
}

public func ==(lhs: ActivityLevelSnapshot, rhs: ActivityLevelSnapshot) -> Bool {
    return lhs.endDateHour == rhs.endDateHour && lhs.endDateMinute == rhs.endDateMinute && lhs.level == rhs.level
}

public func <(lhs: ActivityLevelSnapshot, rhs: ActivityLevelSnapshot) -> Bool {
    if lhs.endDateHour < rhs.endDateHour {
        return true
    } else if lhs.endDateHour > rhs.endDateHour {
        return false
    }
    
    if lhs.endDateMinute < rhs.endDateMinute {
        return true
    } else if lhs.endDateMinute > rhs.endDateMinute {
        return false
    }
    
    return false
}

public struct StepCountSamples {
    public let intervalEndDates: [NSDate]
    public let stepCountMap: [NSDate:(count: Int, level: ActivityLevel)]
    internal let activityLevelSnapshotSetMap: ActivityLevelSnapshotSetMap
    
    public init(intervalEndDates: [NSDate], stepCountMap: [NSDate:(count: Int, level: ActivityLevel)], activityLevelSnapshotSetMap: ActivityLevelSnapshotSetMap) {
        self.intervalEndDates = intervalEndDates
        self.stepCountMap = stepCountMap
        self.activityLevelSnapshotSetMap = activityLevelSnapshotSetMap
    }
}

public struct StepCountStats {
    public let from: ActivityLevelSnapshot
    public let to: ActivityLevelSnapshot
    
    init(from: ActivityLevelSnapshot, to: ActivityLevelSnapshot) {
        self.from = from
        self.to = to
    }
}


