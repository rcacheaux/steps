//  Copyright © 2015 Under Armour. All rights reserved.

import Foundation

extension NSDate {
    class public func dateAtMidnightSixDaysAgo() -> NSDate {
        let dateSixDaysAgo = NSDate().dateBySubtractingNumberOfDays(6)
        return dateSixDaysAgo.dateAtUsersMidnight()
    }
    
    public func dateAtUsersMidnight() -> NSDate {
        let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        let unitFlags: NSCalendarUnit = [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day]
        let dateComponents = calendar.components(unitFlags, fromDate: self)
        return calendar.dateFromComponents(dateComponents)!
    }
    
    public func dateBySubtractingNumberOfDays(numberOfDays: Int) -> NSDate {
        let days = NSTimeInterval(numberOfDays)
        return dateByAddingTimeInterval((-1 * days * 24 * 60 * 60))
    }
    
    public func dateByAddingTwentyFourHours() -> NSDate {
        return dateByAddingTimeInterval((24 * 60 * 60))
    }
    
    class public func dateWithHour(hour: Int, minute: Int) -> NSDate {
        assert(hour >= 0 && hour <= 24 && minute >= 0 && minute <= 60)
        let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        let components = NSDateComponents()
        components.hour = hour
        components.minute = minute
        return calendar.dateFromComponents(components)!
    }
}
