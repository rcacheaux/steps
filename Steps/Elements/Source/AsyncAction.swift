//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

public class AsyncAction: Action {
  private var _executing = false
  private var _finished = false
  
  override public var asynchronous: Bool {
    return true
  }
  
  public func actionCompleted() { } // Abstract method.
  
  override public var executing: Bool {
    get {
      return _executing
    }
    set {
      willChangeValueForKey("isExecuting")
      _executing = newValue
      didChangeValueForKey("isExecuting")
    }
  }
  
  override public var finished: Bool {
    get {
      return _finished
    }
    set {
      willChangeValueForKey("isFinished")
      _finished = newValue
      didChangeValueForKey("isFinished")
    }
  }
  
  override public var completionBlock: (() -> Void)? {
    set {
      super.completionBlock = newValue
    }
    get {
      return {
        super.completionBlock?()
        self.actionCompleted()
      }
    }
  }
  
  override public func start() {
    if cancelled {
      finished = true
      return
    }
    
    executing = true
    autoreleasepool {
      self.run()
    }
  }
  
  public func finishExecutingOperation() {
    executing = false
    finished = true
  }
}
